<?php
set_time_limit(5000);
/**
 * Created by Lê Đình Toản.
 * User: dinhtoan1905@gmail.com
 * Date: 5/4/2017
 * Time: 1:19 PM
 */
ob_start();
require_once dirname(__FILE__) . '/../../bootstrap/bootstrap.php';

use samdark\sitemap\Sitemap;
use samdark\sitemap\Index;

define('URL_DOMAIN',"https://xe.vatgia.com");
define('ROOT_CACHE_DIR', ROOT . '/../public_html/cache/sitemap/');
// create sitemap index file
$files = [];
echo '<pre>';
$max_page = intval(@file_get_contents(ROOT_CACHE_DIR . '/max_page.cfn'));
$arrayFileIndex = [];
$max_loc = 20000;
$finish = false;
$i = 0;
$last_id = 0;
//*


$arr_name_sitemap = [
    2010 => [
        'type' => 2010,
        'name' => 'oto',
    ],
    2020 => [
        'type' => 2020,
        'name' => 'xemay',
    ],
    2050 => [
        'type' => 2050,
        'name' => 'xetai',
    ]
];
$key_type = 2050;
$site_map_name = $arr_name_sitemap[$key_type]['name'];

$finish = false;
$page = 0;
$rew_id = 0;
while($finish === false){
    $filename = "rewrite-". $site_map_name ."-" . $rew_id . ".xml";
    $sitemap = new Sitemap(ROOT_CACHE_DIR . '/' . $filename);
    ///echo "SELECT rew_id,rew_rewrite,rew_date,rew_table,(IF(rew_cat_id > 0,1,0)+IF(rew_cat_id > 0,1,0)+IF(rew_cit_id > 0,1,0)+IF(rew_dis_id > 0,1,0)+IF(rew_ward_id > 0,1,0)+IF(rew_street_id > 0,1,0)+IF(rew_tag_id > 0,1,0)) AS priority FROM rewrites WHERE rew_total_result > 0 ORDER BY rew_id DESC LIMIT " . ($page*$max_loc) . ",$max_loc";
    $db_select = new db_query("SELECT rew_id,rew_url,rew_type FROM rewrite WHERE rew_type = $key_type ORDER BY rew_id DESC LIMIT " . ($page*$max_loc) . ",$max_loc");
    if(mysqli_num_rows($db_select->result) < $max_loc) $finish = true;
    while($row = mysqli_fetch_assoc($db_select->result)){
        /*
        $priority = ((10 - $row["priority"]) / 10);
        if($row["rew_table"] == "investors") $priority = 0.1;
        if($row["rew_table"] == "projects") $priority = 0.6;
        if($priority == 1) $priority  = 0.5;*/
        $rew_url = $row["rew_url"];
        $rew_url = trim($rew_url);
        $sitemap->addItem(URL_DOMAIN . '/' . $rew_url, time(), Sitemap::HOURLY);
        $rew_id = $row["rew_id"];
    }
    if(isset($sitemap)){
        $sitemap->write();
    }
    $page++;
}

//*/
if($last_id > 0){

}
$files = scandir (ROOT_CACHE_DIR . "/");
$index = new Index(ROOT_CACHE_DIR . '/sitemap_dxt.xml');
foreach($files as $file){
    if(strpos($file, 'rewrite') === false) continue;
    if(strpos($file,".xml") !== false && $file != "sitemap.xml") $index->addSitemap(URL_DOMAIN ."/sitemap/" . $file);
}
$index->write();




