<?php
namespace App\Repositories;

use App\Models\Regions as Model;
use App\Repositories\Repository;

class RegionsRepository extends Repository
{
    protected $model;

    public function __construct()
    {
        $this->model = new Model;
    }

    public function getById($id){
        return $this->model->find($id);
    }

    public static function getNameMd5($name){
    	$name 	= strtolower($name);
    	$name 	= removeAccent($name);
    	$name 	= trim($name);
    	$name 	= str_replace(" ", "", $name);

    	return $name;
    }

    public function getByName($name){
    	$name 	= self::getNameMd5($name);

    	if(empty($name)) return false;

    	$data	= $this->model->where('md5', "=", md5($name))->first();

    	return $data;
    }
}
