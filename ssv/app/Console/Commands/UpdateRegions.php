<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\Repositories\RegionsRepository;
class UpdateRegions extends Command
{
    private $url;
    private $table;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'regions:update {id : The ID of the region}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update region';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->url = 'https://gateway.chotot.com/v1/public/web-proxy-api/loadRegions';
        $this->table = 'regions';
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        //echo $this->argument('id');
        $content = file_get_contents($this->url);
        $list = json_decode($content);
        $entities = $list->regionFollowId->entities;
        $arrCityInsert = [];
        $arrAreaInsert = [];
        foreach ($entities->regions as $idR => $row) {
            $geoArr = explode(',', $row->geo);

            $arrCityInsert[$idR] = [
                'id' => (int) $row->id,
                'name' => (string) $row->name,
                'full_name' => (string) $row->name,
                'md5' => md5(RegionsRepository::getNameMd5($row->name)),
                'url' => $row->name_url,
                //'trigrams' => '',
                'area_order' => (string) implode(',', $row->areaOrder),
                'geo' =>  $row->geo,
                'geo_code' => $row->geo_region,
                'geo_lat' => $geoArr[0],
                'geo_long' => $geoArr[1]
            ];
//            continue;
            $area = $row->area;
            foreach ($area as $idA => $rowA){
                $geoArr = explode(',', $rowA->geo);
                $arrAreaInsert[$idA] = [
                    'id' => (int) $rowA->id,
                    'name' => (string) $rowA->name,
                    'full_name' => (string) $rowA->name,
                    'md5' => (string) md5($rowA->name),
                    'url' => (string) $rowA->name_url,
                    'geo' => $rowA->geo,
                    //'trigrams' => '',
                    'geo_code' => $rowA->geo_region,
                    'geo_lat' => (isset($geoArr[0])) ? $geoArr[0] : null,
                    'geo_long' => (isset($geoArr[1])) ? $geoArr[1] : null,
                    'parent_id' => (int) $row->id
                ];
            }

//            dd($arrInsert);
//            exit();
        }
//        dd($arrInsert);
//        exit();
        try {
            DB::table($this->table)->insertOrIgnore($arrCityInsert);
            DB::table($this->table)->insertOrIgnore($arrAreaInsert);
        }catch (\Exception $e){
            echo $e->getMessage();
        }

        return 0;
    }
}
