<?php
namespace App\Models;

use App\Models\Model;

class Regions extends Model
{

	protected $table			= 'regions';
	protected $primaryKey		= 'id';
	protected $guarded			= [''];
	//protected $fillable		= [];
	public $timestamps			= false;
	public $prefix				= '';
	public $defaultFieldsSelect	= ['id', 'name', 'url'];
	const FIELD_FOR_DEFAULT		= ['id', 'name', 'url'];
}
?>

