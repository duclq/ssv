<?php
function remove_magic_quote($string){
	$string	= str_replace("\'", "'", $string);
	$string	= str_replace('\"', '"', $string);
	$string	= str_replace('\&quot;', "&quot;", $string);
	$string	= str_replace("\\\\", "\\", $string);
	$string	= str_replace("<script>", "", $string);
	$string	= str_replace('</script>', "", $string);
	return $string;
}

 function cleanKeywordSearch($string)
 {
	$string = removeHTML($string);
	$string = convertToUnicode($string);
	$string = strval($string);
	$string = str_replace(array(chr(9), chr(10), chr(13)), "", $string);
	$string = mb_strtolower($string, "UTF-8");
	$array_bad_word = array("?", "^", ",", ";", "*", "(", ")", "|", "!", "\\", "@");
	$string = str_replace($array_bad_word, " ", $string);
	$string = str_replace("  ", " ", $string);
	$string = str_replace("  ", " ", $string);
	$string = str_replace("  ", " ", $string);
	$string = str_replace("  ", " ", $string);
	$string = str_replace("  ", " ", $string);
	$string = str_replace("  ", " ", $string);
	$string = str_replace("  ", " ", $string);
	$string = str_replace("  ", " ", $string);
	$string = replaceSphinxMQ($string);

	return trim($string);
}

/**
 * removeAccent()
 *
 * @param mixed $mystring
 * @return
 */
function removeAccent($mystring)
{
	$marTViet = array(
				// Chữ thường
		"à", "á", "ạ", "ả", "ã", "â", "ầ", "ấ", "ậ", "ẩ", "ẫ", "ă", "ằ", "ắ", "ặ", "ẳ", "ẵ",
		"è", "é", "ẹ", "ẻ", "ẽ", "ê", "ề", "ế", "ệ", "ể", "ễ",
		"ì", "í", "ị", "ỉ", "ĩ",
		"ò", "ó", "ọ", "ỏ", "õ", "ô", "ồ", "ố", "ộ", "ổ", "ỗ", "ơ", "ờ", "ớ", "ợ", "ở", "ỡ",
		"ù", "ú", "ụ", "ủ", "ũ", "ư", "ừ", "ứ", "ự", "ử", "ữ",
		"ỳ", "ý", "ỵ", "ỷ", "ỹ",
		"đ", "Đ", "'",
				// Chữ hoa
		"À", "Á", "Ạ", "Ả", "Ã", "Â", "Ầ", "Ấ", "Ậ", "Ẩ", "Ẫ", "Ă", "Ằ", "Ắ", "Ặ", "Ẳ", "Ẵ",
		"È", "É", "Ẹ", "Ẻ", "Ẽ", "Ê", "Ề", "Ế", "Ệ", "Ể", "Ễ",
		"Ì", "Í", "Ị", "Ỉ", "Ĩ",
		"Ò", "Ó", "Ọ", "Ỏ", "Õ", "Ô", "Ồ", "Ố", "Ộ", "Ổ", "Ỗ", "Ơ", "Ờ", "Ớ", "Ợ", "Ở", "Ỡ",
		"Ù", "Ú", "Ụ", "Ủ", "Ũ", "Ư", "Ừ", "Ứ", "Ự", "Ử", "Ữ",
		"Ỳ", "Ý", "Ỵ", "Ỷ", "Ỹ",
		"Đ", "Đ", "'",
 );
	$marKoDau = array(
				/// Chữ thường
		"a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a",
		"e", "e", "e", "e", "e", "e", "e", "e", "e", "e", "e",
		"i", "i", "i", "i", "i",
		"o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o",
		"u", "u", "u", "u", "u", "u", "u", "u", "u", "u", "u",
		"y", "y", "y", "y", "y",
		"d", "D", "",
				//Chữ hoa
		"A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A",
		"E", "E", "E", "E", "E", "E", "E", "E", "E", "E", "E",
		"I", "I", "I", "I", "I",
		"O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O",
		"U", "U", "U", "U", "U", "U", "U", "U", "U", "U", "U",
		"Y", "Y", "Y", "Y", "Y",
		"D", "D", "",
 );

	return str_replace($marTViet, $marKoDau, $mystring);
}

/**
 * removeHTML()
 *
 * @param mixed $string
 * @return
 */
function removeHTML($string)
{
	$string = preg_replace('/<script.*?\>.*?<\/script>/si', ' ', $string);
	$string = preg_replace('/<style.*?\>.*?<\/style>/si', ' ', $string);
	$string = preg_replace('/<.*?\>/si', ' ', $string);
	$string = str_replace('&nbsp;', ' ', $string);

	return $string;
}

/**
 * removeLink()
 *
 * @param mixed $string
 * @return
 */
function removeLink($string)
{
	$string = preg_replace('/<a.*?\>/si', '', $string);
	$string = preg_replace('/<\/a>/si', '', $string);

	return $string;
}

function replaceMQ($str){
		$str = str_replace("\'", "'", $str);
		$str = str_replace("'", "''", $str);
		$str = str_replace("\\", "", $str);
		return $str;
}

function convertToUnicode($string)
{
	$trans = array("á" => "á", "à" => "à", "ả" => "ả", "ã" => "ã", "ạ" => "ạ", "ă" => "ă", "ắ" => "ắ",
		"ằ" => "ằ", "ẳ" => "ẳ", "ẵ" => "ẵ", "ặ" => "ặ", "â" => "â", "ấ" => "ấ", "ầ" => "ầ", "ẩ" => "ẩ",
		"ậ" => "ậ", "ẫ" => "ẫ", "ó" => "ó", "ò" => "ò", "ỏ" => "ỏ", "õ" => "õ", "ọ" => "ọ", "ô" => "ô",
		"ố" => "ố", "ồ" => "ồ", "ổ" => "ổ", "ỗ" => "ỗ", "ộ" => "ộ", "ơ" => "ơ", "ớ" => "ớ", "ờ" => "ờ",
		"ở" => "ở", "ỡ" => "ỡ", "ợ" => "ợ", "ú" => "ú", "ù" => "ù", "ủ" => "ủ", "ũ" => "ũ", "ụ" => "ụ",
		"ư" => "ư", "ứ" => "ứ", "ừ" => "ừ", "ử" => "ử", "ự" => "ự", "ữ" => "ữ", "é" => "é", "è" => "è",
		"ẻ" => "ẻ", "ẽ" => "ẽ", "ẹ" => "ẹ", "ê" => "ê", "ế" => "ế", "ề" => "ề", "ể" => "ể", "ễ" => "ễ",
		"ệ" => "ệ", "í" => "í", "ì" => "ì", "ỉ" => "ỉ", "ĩ" => "ĩ", "ị" => "ị", "ý" => "ý", "ỳ" => "ỳ",
		"ỷ" => "ỷ", "ỹ" => "ỹ", "ỵ" => "ỵ", "đ" => "đ", "Á" => "Á", "À" => "À", "Ả" => "Ả", "Ã" => "Ã",
		"Ạ" => "Ạ", "Ă" => "Ă", "Ắ" => "Ắ", "Ằ" => "Ằ", "Ẳ" => "Ẳ", "Ẵ" => "Ẵ", "Ặ" => "Ặ", "Â" => "Â",
		"Ấ" => "Ấ", "Ầ" => "Ầ", "Ẩ" => "Ẩ", "Ậ" => "Ậ", "Ẫ" => "Ẫ", "Ó" => "Ó", "Ò" => "Ò", "Ỏ" => "Ỏ",
		"Õ" => "Õ", "Ọ" => "Ọ", "Ô" => "Ô", "Ố" => "Ố", "Ồ" => "Ồ", "Ổ" => "Ổ", "Ỗ" => "Ỗ", "Ộ" => "Ộ",
		"Ơ" => "Ơ", "Ớ" => "Ớ", "Ờ" => "Ờ", "Ở" => "Ở", "Ỡ" => "Ỡ", "Ợ" => "Ợ", "Ú" => "Ú", "Ù" => "Ù",
		"Ủ" => "Ủ", "Ũ" => "Ũ", "Ụ" => "Ụ", "Ư" => "Ư", "Ứ" => "Ứ", "Ừ" => "Ừ", "Ử" => "Ử", "Ữ" => "Ữ",
		"Ự" => "Ự", "É" => "É", "È" => "È", "Ẻ" => "Ẻ", "Ẽ" => "Ẽ", "Ẹ" => "Ẹ", "Ê" => "Ê", "Ế" => "Ế",
		"Ề" => "Ề", "Ể" => "Ể", "Ễ" => "Ễ", "Ệ" => "Ệ", "Í" => "Í", "Ì" => "Ì", "Ỉ" => "Ỉ", "Ĩ" => "Ĩ",
		"Ị" => "Ị", "Ý" => "Ý", "Ỳ" => "Ỳ", "Ỷ" => "Ỷ", "Ỹ" => "Ỹ", "Ỵ" => "Ỵ", "Đ" => "Đ",
		"&#225;" => "á", "&#224;" => "à", "&#7843;" => "ả", "&#227;" => "ã", "&#7841;" => "ạ", "&#259;" => "ă",
		"&#7855;" => "ắ", "&#7857;" => "ằ", "&#7859;" => "ẳ", "&#7861;" => "ẵ", "&#7863;" => "ặ", "&#226;" => "â",
		"&#7845;" => "ấ", "&#7847;" => "ầ", "&#7849;" => "ẩ", "&#7853;" => "ậ", "&#7851;" => "ẫ", "&#243;" => "ó",
		"&#242;" => "ò", "&#7887;" => "ỏ", "&#245;" => "õ", "&#7885;" => "ọ", "&#244;" => "ô", "&#7889;" => "ố",
		"&#7891;" => "ồ", "&#7893;" => "ổ", "&#7895;" => "ỗ", "&#7897;" => "ộ", "&#417;" => "ơ", "&#7899;" => "ớ",
		"&#7901;" => "ờ", "&#7903;" => "ở", "&#7905;" => "ỡ", "&#7907;" => "ợ", "&#250;" => "ú", "&#249;" => "ù",
		"&#7911;" => "ủ", "&#361;" => "ũ", "&#7909;" => "ụ", "&#432;" => "ư", "&#7913;" => "ứ", "&#7915;" => "ừ",
		"&#7917;" => "ử", "&#7921;" => "ự", "&#7919;" => "ữ", "&#233;" => "é", "&#232;" => "è", "&#7867;" => "ẻ",
		"&#7869;" => "ẽ", "&#7865;" => "ẹ", "&#234;" => "ê", "&#7871;" => "ế", "&#7873;" => "ề", "&#7875;" => "ể",
		"&#7877;" => "ễ", "&#7879;" => "ệ", "&#237;" => "í", "&#236;" => "ì", "&#7881;" => "ỉ", "&#297;" => "ĩ",
		"&#7883;" => "ị", "&#253;" => "ý", "&#7923;" => "ỳ", "&#7927;" => "ỷ", "&#7929;" => "ỹ", "&#7925;" => "ỵ",
		"&#273;" => "đ", "&#193;" => "Á", "&#192;" => "À", "&#7842;" => "Ả", "&#195;" => "Ã", "&#7840;" => "Ạ",
		"&#258;" => "Ă", "&#7854;" => "Ắ", "&#7856;" => "Ằ", "&#7858;" => "Ẳ", "&#7860;" => "Ẵ", "&#7862;" => "Ặ",
		"&#194;" => "Â", "&#7844;" => "Ấ", "&#7846;" => "Ầ", "&#7848;" => "Ẩ", "&#7852;" => "Ậ", "&#7850;" => "Ẫ",
		"&#211;" => "Ó", "&#210;" => "Ò", "&#7886;" => "Ỏ", "&#213;" => "Õ", "&#7884;" => "Ọ", "&#212;" => "Ô",
		"&#7888;" => "Ố", "&#7890;" => "Ồ", "&#7892;" => "Ổ", "&#7894;" => "Ỗ", "&#7896;" => "Ộ", "&#416;" => "Ơ",
		"&#7898;" => "Ớ", "&#7900;" => "Ờ", "&#7902;" => "Ở", "&#7904;" => "Ỡ", "&#7906;" => "Ợ", "&#218;" => "Ú",
		"&#217;" => "Ù", "&#7910;" => "Ủ", "&#360;" => "Ũ", "&#7908;" => "Ụ", "&#431;" => "Ư", "&#7912;" => "Ứ",
		"&#7914;" => "Ừ", "&#7916;" => "Ử", "&#7918;" => "Ữ", "&#7920;" => "Ự", "&#201;" => "É", "&#200;" => "È",
		"&#7866;" => "Ẻ", "&#7868;" => "Ẽ", "&#7864;" => "Ẹ", "&#202;" => "Ê", "&#7870;" => "Ế", "&#7872;" => "Ề",
		"&#7874;" => "Ể", "&#7876;" => "Ễ", "&#7878;" => "Ệ", "&#205;" => "Í", "&#204;" => "Ì", "&#7880;" => "Ỉ",
		"&#296;" => "Ĩ", "&#7882;" => "Ị", "&#221;" => "Ý", "&#7922;" => "Ỳ", "&#7926;" => "Ỷ", "&#7928;" => "Ỹ",
		"&#7924;" => "Ỵ", "&#272;" => "Đ"
	 );
	$string = strtr($string, $trans);
	$string = mb_convert_encoding($string, "UTF-8", "UTF-8");

	return $string;
}

function base64url_encode($data)
{
	return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');
}

function base64url_decode($data)
{
	return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT));
}

/**
Base64 để hiển thị trên URL để tránh lỗi
*/
function base64_url_encode($input){
	return strtr(base64_encode($input), '+/=', '_,-');
}

function base64_url_decode($input) {
	return base64_decode(strtr($input, '_,-', '+/='));
}

function splitPhoneNumber($string){

	$str_tmp = str_replace(array(" - ", " . "), " / ", $string);
	$str_tmp = preg_replace('/\s/', '', $str_tmp);

	$pattern    = '/(\d{6,}(?!\d)|(?<!\d)\d{6,}|(\(|\d|\.|-|,|\)){6,})/';

	preg_match_all($pattern, $str_tmp, $match);
		//print_r($match[0]);

		$result = array();// Mang luu lai ket qua tra ve
		foreach($match[0] as $key => $value){
			 $result[$key]["socu"]   = removeCharPhoneNumber($value);
			 $result[$key]["somoi"]  = preg_replace('/\D/', '', $value);
		}

		return $result;

 }

 function removeCharPhoneNumber($string){

	$length     = mb_strlen($string, "UTF-8");

	$start_char = 0;
		//Remove các ký tự ko phải số ở đầu
	for($i=0; $i<$length; $i++){
		$char   = mb_substr($string, $i, 1, "UTF-8");
		if(($char == "(") || (is_numeric($char))) break;
		$start_char = $i+1;
	 }

	 $end_char   = $length;
			//Remove các ký tự ko phải số ở cuối
	 for($i=$length; $i>=0; $i--){
			$char   = mb_substr($string, $i-1, 1, "UTF-8");
			if(is_numeric($char)) break;
			$end_char   = $i-1;
	 }
			//Cắt chuỗi
	 $string     = mb_substr($string, $start_char, ($end_char - $start_char), "UTF-8");

 return $string;

}

function convert_to_unicode($string){
	$array_string = array(
		 "à","á","ạ","ả","ã","â","ầ","ấ","ậ","ẩ","ẫ","ă","ằ","ắ","ặ","ẳ","ẵ",
		 "è","é","ẹ","ẻ","ẽ","ê","ề","ế","ệ","ể","ễ",
		 "ì","í","ị","ỉ","ĩ",
		 "ò","ó","ọ","ỏ","õ","ô","ồ","ố","ộ","ổ","ỗ","ơ","ờ","ớ","ợ","ở","ỡ",
		 "ù","ú","ụ","ủ","ũ","ư","ừ","ứ","ự","ử","ữ",
		 "ỳ","ý","ỵ","ỷ","ỹ",
		 "đ",
		 "À","Á","Ạ","Ả","Ã","Â","Ầ","Ấ","Ậ","Ẩ","Ẫ","Ă","Ằ","Ắ","Ặ","Ẳ","Ẵ",
		 "È","É","Ẹ","Ẻ","Ẽ","Ê","Ề","Ế","Ệ","Ể","Ễ",
		 "Ì","Í","Ị","Ỉ","Ĩ",
		 "Ò","Ó","Ọ","Ỏ","Õ","Ô","Ồ","Ố","Ộ","Ổ","Ỗ","Ơ","Ờ","Ớ","Ợ","Ở","Ỡ",
		 "Ù","Ú","Ụ","Ủ","Ũ","Ư","Ừ","Ứ","Ự","Ử","Ữ",
		 "Ỳ","Ý","Ỵ","Ỷ","Ỹ",
		 "Đ");

	$array_replace  = array(
		 "à","á","ạ","ả","ã","â","ầ","ấ","ậ","ẩ","ẫ","ă","ằ","ắ","ặ","ẳ","ẵ",
		 "è","é","ẹ","ẻ","ẽ","ê","ề","ế","ệ","ể","ễ",
		 "ì","í","ị","ỉ","ĩ",
		 "ò","ó","ọ","ỏ","õ","ô","ồ","ố","ộ","ổ","ỗ","ơ","ờ","ớ","ợ","ở","ỡ",
		 "ù","ú","ụ","ủ","ũ","ư","ừ","ứ","ự","ử","ữ",
		 "ỳ","ý","ỵ","ỷ","ỹ",
		 "đ",
		 "À","Á","Ạ","Ả","Ã","Â","Ầ","Ấ","Ậ","Ẩ","Ẫ","Ă","Ằ","Ắ","Ặ","Ẳ","Ẵ",
		 "È","É","Ẹ","Ẻ","Ẽ","Ê","Ề","Ế","Ệ","Ể","Ễ",
		 "Ì","Í","Ị","Ỉ","Ĩ",
		 "Ò","Ó","Ọ","Ỏ","Õ","Ô","Ồ","Ố","Ộ","Ổ","Ỗ","Ơ","Ờ","Ớ","Ợ","Ở","Ỡ",
		 "Ù","Ú","Ụ","Ủ","Ũ","Ư","Ừ","Ứ","Ự","Ử","Ữ",
		 "Ỳ","Ý","Ỵ","Ỷ","Ỹ",
		 "Đ");

	return str_replace($array_string, $array_replace, $string);
}

function replace_double_space($string, $char=" "){
	$i      = 0;
	$max    = 10;
	if($char == "") return $string;
	while(mb_strpos($string, $char.$char, 0, "UTF-8") !== false){
		$i++;
		$string = str_replace($char.$char, $char, $string);
		if($i >= $max) break;
 }
 return trim($string);
}

function check_email_address($email) {
	//First, we check that there's one @ symbol, and that the lengths are right
	if(!@preg_match("^[^@]{1,64}@[^@]{1,255}$", $email)){
			//Email invalid because wrong number of characters in one section, or wrong number of @ symbols.
			return false;
	}
	//Split it into sections to make life easier
	$email_array = explode("@", $email);
	$local_array = explode(".", $email_array[0]);
	for($i = 0; $i < sizeof($local_array); $i++){
			if(!preg_match("^(([A-Za-z0-9!#$%&'*+/=?^_`{|}~-][A-Za-z0-9!#$%&'*+/=?^_`{|}~\.-]{0,63})|(\"[^(\\|\")]{0,62}\"))$", $local_array[$i])){
					return false;
			}
	}
	if(!preg_match("^\[?[0-9\.]+\]?$", $email_array[1])){
	//Check if domain is IP. If not, it should be valid domain name
			$domain_array = explode(".", $email_array[1]);
			if(sizeof($domain_array) < 2){
					return false; // Not enough parts to domain
			}
			for($i = 0; $i < sizeof($domain_array); $i++){
					if (!preg_match("^(([A-Za-z0-9][A-Za-z0-9-]{0,61}[A-Za-z0-9])|([A-Za-z0-9]+))$", $domain_array[$i])){
							return false;
					}
			}
	}
	return true;
}

/**
 * Format LoginPhon
 *
 * Đưa hết về dạng 09xxxxxxxx hoặc 01xxxxxxxxx hoặc 08xxxxxxxx
 */
function formatLoginPhone($phone)
{

		$phone = str_replace('+84', '0', $phone);

		$phone = preg_replace("/[^A-Za-z0-9 ]/", '', $phone);


		//Check xem có bắt đầu bằng số 0?
		if (substr($phone, 0, 1) == '0') {
				//09 thì là 10 số --- 01 thì là 11 số
				if (
						(substr($phone, 0, 2) == '09' && strlen($phone) == 10)
						|| (substr($phone, 0, 2) == '01' && strlen($phone) == 11)
						|| (substr($phone, 0, 2) == '08' && strlen($phone) == 10)
				) {
						return $phone;
				}
		}
		return false;
}

// Đổi ngày ra Hôm nay, Hôm qua....
function today_yesterday_v2($intTime, $type=0){
 // dd($intTime, date('dmY', $intTime));
	$compare_time   =  time() - $intTime;

	$today    = getdate();
	$yesterday  = getdate(strtotime("yesterday"));
	$ct     = getdate($compare_time);

	if($type == 0){
		// Nếu thời gian nhỏ hơn 10h thì show kiểu "10 giờ 30 phút" trước
		if($compare_time / 3600 <= 24*30) return generateDuration($compare_time, 3, "1 phút") . " trước";
	}

	// Kiểm tra so với hôm nay
	if($today["mday"]==$ct["mday"] && $today["month"]==$ct["month"] && $today["year"]==$ct["year"]) return "Hôm nay, lúc " . date("H:i", $compare_time);
	if($yesterday["mday"]==$ct["mday"] && $yesterday["month"]==$ct["month"] && $yesterday["year"]==$ct["year"]) return "Hôm qua, lúc " . date("H:i",$compare_time);
	// Nếu không trùng thì return lại
	return date("d/m/Y - H:i", $intTime);
}

function generateDuration($int_time, $type=4, $default="", $limit_param=0, $time=array()){
	$strReturn  = "";
	$arrTime    = array (86400  => "ngày",
								3600  => "giờ",
								60    => "phút",
								1   => "giây",
								);
	if(is_array($time) && count($time) > 0) $arrTime  = $time;
	$i = 0;
	$j  = 0;
	foreach($arrTime as $key => $value){
		$i++;
		if($int_time >= $key){
			$j++;
			$strReturn .= " " . format_number(intval($int_time/$key)) . " " . $value;
			$int_time = $int_time % $key;
			if($limit_param > 0 && $j >= $limit_param) break;
		}
		if($i >= $type) break;
	}
	if($strReturn == "") $strReturn = $default;
	return trim($strReturn);
}

function generateDurationShort($int_time, $default="1 phút"){
	$strReturn  = $default;
	$arrTime    = array (
		31536000 => "năm",
		86400  => "ngày",
		3600  => "giờ",
		60    => "phút",
	);
	foreach($arrTime as $key => $value){
		if($int_time >= $key){
			$strReturn = format_number(intval($int_time/$key)) . " " . $value;
			return $strReturn;
		}
	}
	return $strReturn;
}

function format_number($number, $num_decimal=2, $edit=0){

	$sep    = ($edit == 0 ? array(",", ".") : array(".", ""));
	$stt    = -1;
	$return = number_format( floatval($number), $num_decimal, $sep[0], $sep[1]);
	for($i=$num_decimal; $i>0; $i--){
		$stt++;
		if(intval(substr($return, -$i, $i)) == 0){
			$return = number_format( floatval($number), $stt, $sep[0], $sep[1]);
			break;
		}
	}
	return $return;
}

function convert_utf82utf8($str = '',  $removeAccent = 0){
	 $new_str = '';
	 $marTViet=array(
		// Chữ thường
		"à","á","ạ","ả","ã","â","ầ","ấ","ậ","ẩ","ẫ","ă","ằ","ắ","ặ","ẳ","ẵ",
		"è","é","ẹ","ẻ","ẽ","ê","ề","ế","ệ","ể","ễ",
		"ì","í","ị","ỉ","ĩ",
		"ò","ó","ọ","ỏ","õ","ô","ồ","ố","ộ","ổ","ỗ","ơ","ờ","ớ","ợ","ở","ỡ",
		"ù","ú","ụ","ủ","ũ","ư","ừ","ứ","ự","ử","ữ",
		"ỳ","ý","ỵ","ỷ","ỹ",
		"đ","Đ",
		"À","Á","Ạ","Ả","Ã","Â","Ầ","Ấ","Ậ","Ẩ","Ẫ","Ă","Ằ","Ắ","Ặ","Ẳ","Ẵ",
		"È","É","Ẹ","Ẻ","Ẽ","Ê","Ề","Ế","Ệ","Ể","Ễ",
		"Ì","Í","Ị","Ỉ","Ĩ",
		"Ò","Ó","Ọ","Ỏ","Õ","Ô","Ồ","Ố","Ộ","Ổ","Ỗ","Ơ","Ờ","Ớ","Ợ","Ở","Ỡ",
		"Ù","Ú","Ụ","Ủ","Ũ","Ư","Ừ","Ứ","Ự","Ử","Ữ",
		"Ỳ","Ý","Ỵ","Ỷ","Ỹ",
		"Đ","Đ"
		);
	$arrTohop   = array( 0 => 'à', 1 => 'á', 2 => 'ạ', 3 => 'ả', 4 => 'ã', 5 => 'â', 6 => 'ầ', 7 => 'ấ', 8 => 'ậ', 9 => 'ẩ', 10 => 'ẫ', 11 => 'ă', 12 => 'ằ', 13 => 'ắ', 14 => 'ặ', 15 => 'ẳ', 16 => 'ẵ', 17 => 'è', 18 => 'é', 19 => 'ẹ', 20 => 'ẻ', 21 => 'ẽ', 22 => 'ê', 23 => 'ề', 24 => 'ế', 25 => 'ệ', 26 => 'ể', 27 => 'ễ', 28 => 'ì', 29 => 'í', 30 => 'ị', 31 => 'ỉ', 32 => 'ĩ', 33 => 'ò', 34 => 'ó', 35 => 'ọ', 36 => 'ỏ', 37 => 'õ', 38 => 'ô', 39 => 'ồ', 40 => 'ố', 41 => 'ộ', 42 => 'ổ', 43 => 'ỗ', 44 => 'ơ', 45 => 'ờ', 46 => 'ớ', 47 => 'ợ', 48 => 'ở', 49 => 'ỡ', 50 => 'ù', 51 => 'ú', 52 => 'ụ', 53 => 'ủ', 54 => 'ũ', 55 => 'ư', 56 => 'ừ', 57 => 'ứ', 58 => 'ự', 59 => 'ử', 60 => 'ữ', 61 => 'ỳ', 62 => 'ý', 63 => 'ỵ', 64 => 'ỷ', 65 => 'ỹ', 66 => 'đ', 67 => 'Đ', 68 => 'À', 69 => 'Á', 70 => 'Ạ', 71 => 'Ả', 72 => 'Ã', 73 => 'Â', 74 => 'Ầ', 75 => 'Ấ', 76 => 'Ậ', 77 => 'Ẩ', 78 => 'Ẫ', 79 => 'Ă', 80 => 'Ằ', 81 => 'Ắ', 82 => 'Ặ', 83 => 'Ẳ', 84 => 'Ẵ', 85 => 'È', 86 => 'É', 87 => 'Ẹ', 88 => 'Ẻ', 89 => 'Ẽ', 90 => 'Ê', 91 => 'Ề', 92 => 'Ế', 93 => 'Ệ', 94 => 'Ể', 95 => 'Ễ', 96 => 'Ì', 97 => 'Í', 98 => 'Ị', 99 => 'Ỉ', 100 => 'Ĩ', 101 => 'Ò', 102 => 'Ó', 103 => 'Ọ', 104 => 'Ỏ', 105 => 'Õ', 106 => 'Ô', 107 => 'Ồ', 108 => 'Ố', 109 => 'Ộ', 110 => 'Ổ', 111 => 'Ỗ', 112 => 'Ơ', 113 => 'Ờ', 114 => 'Ớ', 115 => 'Ợ', 116 => 'Ở', 117 => 'Ỡ', 118 => 'Ù', 119 => 'Ú', 120 => 'Ụ', 121 => 'Ủ', 122 => 'Ũ', 123 => 'Ư', 124 => 'Ừ', 125 => 'Ứ', 126 => 'Ự', 127 => 'Ử', 128 => 'Ữ', 129 => 'Ỳ', 130 => 'Ý', 131 => 'Ỵ', 132 => 'Ỷ', 133 => 'Ỹ', 134 => 'Đ', 135 => 'Đ' );
	 if($str != ''){
			$new_str = str_replace($arrTohop, $marTViet, $str);
	 }

	 if($removeAccent == 1){
			$new_str = strtolower(removeAccent($new_str));
	 }

	 return $new_str;
}
/**
 * ham lay so ngay trong thang
*/
function days_in_month($month, $year) {
	 return $month == 2 ? ($year % 4 ? 28 : ($year % 100 ? 29 : ($year % 400 ? 28 : 29))) : (($month - 1) % 7 % 2 ? 30 : 31);
}
/**
 * Convert string to time
*/
function convertDateTime($strDate = "", $strTime = ""){
	 //Break string and create array date time
	 $strDate			= str_replace("/", "-", $strDate);
	 $strDateArray	= explode("-", $strDate);
	 $countDateArr	= count($strDateArray);
	 $strTime			= str_replace("-", ":", $strTime);
	 $strTimeArray	= explode(":", $strTime);
	 $countTimeArr	= count($strTimeArray);
	 //Get Current date time
	 $today			= getdate();
	 $day				= $today["mday"];
	 $mon				= $today["mon"];
	 $year				= $today["year"];
	 $hour				= $today["hours"];
	 $min				= $today["minutes"];
	 $sec				= $today["seconds"];
	 //Get date array
	 switch($countDateArr){
			case 2:
				 $day		= intval($strDateArray[0]);
				 $mon		= intval($strDateArray[1]);
				 break;
			case $countDateArr >= 3:
				 $day		= intval($strDateArray[0]);
				 $mon		= intval($strDateArray[1]);
				 $year 	= intval($strDateArray[2]);
				 break;
	 }
	 //Get time array
	 switch($countTimeArr){
			case 2:
				 $hour		= intval($strTimeArray[0]);
				 $min		= intval($strTimeArray[1]);
				 break;
			case $countTimeArr >= 3:
				 $hour		= intval($strTimeArray[0]);
				 $min		= intval($strTimeArray[1]);
				 $sec		= intval($strTimeArray[2]);
				 break;
	 }
	 //Return date time integer
	 if(@mktime($hour, $min, $sec, $mon, $day, $year) == -1) return $today[0];
	 else return mktime($hour, $min, $sec, $mon, $day, $year);

}

function cut_string2($str, $length, $char="..."){
	//Nếu chuỗi cần cắt nhỏ hơn $length thì return luôn
	$strlen	= mb_strlen($str, "UTF-8");
	if($strlen <= $length) return $str;

	//Cắt chiều dài chuỗi $str tới đoạn cần lấy
	$substr	= mb_substr($str, 0, $length, "UTF-8");
	if(mb_substr($str, $length, 1, "UTF-8") == " ") return $substr . $char;

	//Xác định dấu " " cuối cùng trong chuỗi $substr vừa cắt
	$strPoint= mb_strrpos($substr, " ", $length);

	//Return string
	if($strPoint < $length - 20) return $substr . $char;
	else return mb_substr($substr, 0, $strPoint, "UTF-8") . $char;
}

function replace_fck($string, $type=0){
	$array_fck	= array ("&Agrave;", "&Aacute;", "&Acirc;", "&Atilde;", "&Egrave;", "&Eacute;", "&Ecirc;", "&Igrave;", "&Iacute;", "&Icirc;",
								"&Iuml;", "&ETH;", "&Ograve;", "&Oacute;", "&Ocirc;", "&Otilde;", "&Ugrave;", "&Uacute;", "&Yacute;", "&agrave;",
								"&aacute;", "&acirc;", "&atilde;", "&egrave;", "&eacute;", "&ecirc;", "&igrave;", "&iacute;", "&ograve;", "&oacute;",
								"&ocirc;", "&otilde;", "&ugrave;", "&uacute;", "&ucirc;", "&yacute;",
								);
	$array_text	= array ("À", "Á", "Â", "Ã", "È", "É", "Ê", "Ì", "Í", "Î",
								"Ï", "Ð", "Ò", "Ó", "Ô", "Õ", "Ù", "Ú", "Ý", "à",
								"á", "â", "ã", "è", "é", "ê", "ì", "í", "ò", "ó",
								"ô", "õ", "ù", "ú", "û", "ý",
								);
	if($type == 1) $string = str_replace($array_fck, $array_text, $string);
	else $string = str_replace($array_text, $array_fck, $string);
	return $string;
}

function htmlspecialbo($str){
	$arrDenied	= array('<', '>', '"');
	$arrReplace	= array('&lt;', '&gt;', '&quot;');
	$str = str_replace($arrDenied, $arrReplace, $str);
	return $str;
}

function convertUrlGallery($string) {
	$string = strval($string);
	if (empty($string)) return '';
	if (preg_match_all('/(https?:)?\/+g.vatgia.vn\/gallery_img\/\d+\/([^"]+)/si', $string, $match)) {
		foreach ($match[0] as $key => $value) {
			if (isset($match[2][$key])) {
				if (preg_match('/^medium_/', $match[2][$key])) {
					$pic_name = str_replace('medium_', '', $match[2][$key]);
					$url = pictureProductThumb($pic_name, 200);
				} elseif (preg_match('/^small_/', $match[2][$key])) {
					$pic_name = str_replace('small_', '', $match[2][$key]);
					$url = pictureProductThumb($pic_name, 80);
				} else {
					$url = pictureProductThumb($match[2][$key]);
				}
				$string = str_replace($value, $url, $string);
			}

		}
	}
	return $string;
}

function convertListToArray($string, $return_null_array = false, $df_null_value = 0){
	// Bẻ $string để intval lại
	$arrTemp	= explode(",", $string);
	$array_id	= array();
	foreach($arrTemp as $key => $value) $array_id[] = intval($value);

	//Nếu ra array rỗng và !$return_null_array không cho trả lại array rỗng thì gán cho giá trị trả về 1 phần tử df
	if(count($array_id) == 0 && !$return_null_array) $array_id = array($df_null_value);
	return $array_id;
}

function convertArrayToList($array_input, $key="", $limit=0){
	$string_return	= "0";
	$arrTemp			= array();
	$i					= 0;
	foreach($array_input as $k => $v){
		$i++;
		if($key === true) $arrTemp[]	= intval($k);
		elseif($key == "") $arrTemp[]	= intval($v);
		else $arrTemp[]	= intval(@$v[trim($key)]);
		if($limit > 0 && $i >= $limit) break;
	}
	//Loại bỏ phần tử trùng
	$arrTemp	= array_unique($arrTemp);
	if(count($arrTemp) > 0) $string_return = implode(",", $arrTemp);
	return $string_return;
}

function checkData($arrField = array(), $data = array())
{
	$arrReturn 	=	array();
	foreach($arrField as $key => $value)
	{
		if(count($value) < 1)
			continue;

		$value 	=	array_values($value);
		$temp 	=	isset($data[$key]) ? $data[$key] : @$value[1];
		switch($value[0])
		{
			case 	'str'	:
				$temp	=	convert_utf82utf8(strval($temp));
				break;
			case 	'int' :
				$temp	=	(int)$temp;
				break;
			case 	'flo' :
				$temp	=	floatval($temp);
				break;
			case 	'dbl' :
				$temp	=	doubleval($temp);
				break;
			case 	'arr'	:
				$temp	=	is_array($temp) ? $temp : array();
				break;
			case 	'arrint'	:
				if(is_numeric($temp) && $temp > 0)
					$temp 	=	array(intval($temp));
				else
				{
					$temp	=	is_array($temp) ? $temp : array();
					$temp =	array_map('intval', $temp);
				}
				break;
		}

		$arrReturn[$key]	=	$temp;
	}

	return $arrReturn;
}

function bigIntval($value) {
	$value = trim($value);
	if (ctype_digit($value)) {
		return $value;
	}
	$value = preg_replace("/[^0-9](.*)$/", '', $value);
	if (ctype_digit($value)) {
		return $value;
	}
	return 0;
}

//Loại bỏ các ký tự \' và \"
function removeMagicQuote($string){
	$string	= str_replace("\'", "'", $string);
	$string	= str_replace('\"', '"', $string);
	$string	= str_replace('\&quot;', "&quot;", $string);
	$string	= str_replace("\\\\", "\\", $string);
	$string	= str_replace("<script>", "", $string);
	$string	= str_replace('</script>', "", $string);
	return $string;
}

function convertListToListId($string, $do_not_get_zero_value=0, $limit=0){
	// Bẻ $string để intval lại
	$arrTemp	= explode(",", $string);
	$list_id	= "";
	$i			= 0;
	$arrCheck	= [];
	foreach($arrTemp as $key => $value){
		$i++;
		$value	= intval($value);
		if($do_not_get_zero_value == 1 && $value <= 0) continue;
		if(isset($arrCheck[$value])) continue;
		$arrCheck[$value]	= 1;
		$list_id .= "," . $value;
		if($limit > 0 && $i >= $limit) break;
	}
	// Remove dấu , đầu tiên
	if(strlen($list_id) > 0) $list_id = substr($list_id, 1);
	else $list_id	= 0;
	return $list_id;
}

function removeTitle($string,$keyReplace = "/"){
	$string	=	removeAccent($string);
	$string 	=  trim(preg_replace("/[^A-Za-z0-9]/i"," ",$string)); // khong dau
	$string 	=  str_replace(" ","-",$string);
	$string	=	str_replace("--","-",$string);
	$string	=	str_replace("--","-",$string);
	$string	=	str_replace("--","-",$string);
	$string	=	str_replace("--","-",$string);
	$string	=	str_replace("--","-",$string);
	$string	=	str_replace("--","-",$string);
	$string	=	str_replace("--","-",$string);
	$string	=	str_replace($keyReplace,"-",$string);
	return strtolower($string);
}

function transformer_item(&$item, \League\Fractal\TransformerAbstract $transformer, $includes = [], $meta = [])
{
	$manager	= new \League\Fractal\Manager();
	$manager->setSerializer(new App\Helper\Transformer\DataArraySerializer());
	$resource	= new \League\Fractal\Resource\Item($item, $transformer);
	if(is_array($includes)){
		foreach($includes as $key => $value){
			if(trim($value) == '') unset($includes[$key]);
		}
	}

	if ($includes) {
		$manager->parseIncludes($includes);
	}

	if ($meta) {
		$resource->setMeta($meta);
	}

	$vars	= $manager->createData($resource)->toArray();

	return $vars;
}

function transformer_collection(&$items, \League\Fractal\TransformerAbstract $transformer, $includes = [], $meta = []){
	$manager	= new \League\Fractal\Manager();
	$manager->setSerializer(new App\Helper\Transformers\DataArraySerializer());
	$resource	= new \League\Fractal\Resource\Collection($items, $transformer);

	if(is_array($includes)){
		foreach($includes as $key => $value)
		{
			if(trim($value) == '') unset($includes[$key]);
		}
	}
	if ($includes) {
		$manager->parseIncludes($includes);
	}

	if ($meta) {
		$resource->setMeta($meta);
	}

	$vars	= $manager->createData($resource)->toArray();

 return $vars;
}

function transformer_collection_paginator(&$items, \League\Fractal\TransformerAbstract $transformer, \League\Fractal\Pagination\PaginatorInterface $paginator, $includes = [], $meta = []){
	$manager	= new \League\Fractal\Manager();
	$resource	= new \League\Fractal\Resource\Collection($items, $transformer);

	$resource->setPaginator($paginator);
	if(is_array($includes)){
		foreach($includes as $key => $value){
			if(trim($value) == '') unset($includes[$key]);
		}
	}

	if ($includes) {
		$manager->parseIncludes($includes);
	}

	if ($meta) {
		$resource->setMeta($meta);
	}

	$vars	= $manager->createData($resource)->toArray();

	return $vars;
}


function getURL($queryString=1, $varDenied='', $varAllowed='', $addParam = array()){

	$url = url()->full();

	if($queryString == 0){
		return url()->current();
	}

	if($varDenied != '' || $varAllowed != ''){
		$url_temp = explode('?', $url);
		$arrQueryString 	= $addParam;
		if(isset($url_temp[1]) && $url_temp[1]){
			$dataGetTemp		= explode('&', $url_temp[1]);
			$dataGet 			= [];
			foreach ($dataGetTemp as $value) {
				$temp = explode('=', $value);
				if(count($temp) == 2){
					$k = $temp[0];
					$v = $temp[1];
					$dataGet[$k] = $v;
				}
			}

			$arrVarDenied	= explode('|', $varDenied);
			$arrVarAllowed	= explode('|', $varAllowed);

			foreach ($dataGet as $k => $v) {
				if(array_search($k, $arrVarDenied) === false && ($varAllowed == "" || array_search($k, $arrVarAllowed) !== false)){
					$arrQueryString[$k] = $v;
				}
			}
		}

		$i = 0;
		$url_query_string = '';
		foreach($arrQueryString as $k => $v){
			$i++;
			if($i > 1) $url_query_string .= "&";
			$url_query_string .= $k . '=' . @urlencode($v);
		}

		//Loại \" -> ", \' => ', \\ =? \
		$url_query_string = str_replace(urlencode('\"'), urlencode('"'), $url_query_string);
		$url_query_string = str_replace(urlencode("\'"), urlencode("'"), $url_query_string);
		$url_query_string = str_replace(urlencode("\\\\"), urlencode("\\"), $url_query_string);

		if($url_query_string){
			$url	= strval($url_temp[0] . "?" . $url_query_string);
		}
	}

	return $url;
}