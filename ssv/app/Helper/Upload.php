<?php
namespace App\Helper;
use Validator;
use Request;
use Config;
use Storage;
class Upload
{
	public static $typeUpload		= ['category', 'product', 'news', 'static', 'banner', 'menu', 'partner', 'config', 'other', 'category'];
	public static $extension 		= ['png','jpg','jpeg','gif'];
    public function __construct(){

    }

    public static function store($fileUpload, $type, $resize = false, $dataResize = []){
    	$dataReturn 	= ['status' => false, 'msg' => '', 'filename' => '', 'url' => '', 'filesize' => 0, 'width' => 0, 'height' => 0];
    	$rulesValidate	= [
							'type'	=> 'bail|required|in:' . implode(',', self::$typeUpload),
							'image'	=> 'bail|required|max:10240',
    					];
    	$data 			= [
    						'type'  => $type,
    						'image' => $fileUpload
    					];
    	$messagesValidate = [
    						'type.in' => 'Type upload invalid'
    						];
    	$validator 	=	Validator::make($data, $rulesValidate, $messagesValidate);

    	$error 	= '';
		if ($validator->fails()) {
			$errors = $validator->errors();
			foreach ($data as $key => $value) {
				if($errors->first($key)) $dataReturn['msg'] .= $errors->first($key);
			}
		}else{
			// Kiểm tra file có tồn tại không
			if ($fileUpload->isValid()) {
				$extension = $fileUpload->extension();
				$filesize = $fileUpload->getSize();

				if(!in_array($extension, self::$extension)){
					$dataReturn['msg'] 	= 'The extension does not match! Only accept extensions: ' . implode(",", self::$extension);
				}else{
					$filename				= self::generate_name($extension);
					// Uploadfile

					$fileUpload->storeAs(self::getPathUpload($type), $filename);

					if($fileUpload->isValid()){
						$dataReturn['status']	= true;
						$dataReturn['filename']	= $filename;
						$dataReturn['filesize']	= $filesize;
						$dataReturn['url']		= self::getUrlImage($type, $filename);
						list($width, $height)	= @getimagesize($dataReturn['url']);
						$dataReturn['width']		= $width;
						$dataReturn['height']	= $height;

						// Nếu có resize
						if($resize){
							$storagePath	= Storage::getDriver()->getAdapter()->getPathPrefix();
							$path			= $storagePath . self::getPathUpload($type);
							// Lấy cấu hình
							$width_resize 	= isset($dataResize['width']) ? $dataResize['width'] : 0;
							$height_resize 	= isset($dataResize['height']) ? $dataResize['height'] : 0;
							$quanlity 		= isset($dataResize['quanlity']) ? $dataResize['quanlity'] : 90;
							if($quanlity > 100) $quanlity = 100;
							if($quanlity <= 0) $quanlity = 90;
							$prefix 		= isset($dataResize['prefix']) ? $dataResize['prefix'] : "thumbnail_";
							if($width_resize > 0 && $height_resize > 0){
								self::resize_image($path, $filename, $width_resize, $height_resize, $prefix, "", $quanlity);
							}
						}
					}else{
						$dataReturn['msg'] = 'Upload file fail !';
					}
				}
			}else{
				$dataReturn['msg'] = 'Upload file does not exist';
			}
		}

		return $dataReturn;

    }

    public static function getUrlImage($type, $filename){
    	$filename = trim($filename);
    	if($type == '' || $filename == '') return '';
    	return asset('storage/' . self::getPathUpload($type) . $filename);
    }

    public static function getPathUpload($type){
    	$path 	= '';
    	switch ($type) {
    		case 'category':
    			$path 	= Config('upload.category');
    			break;

    		case 'product':
    			$path 	= Config('upload.product');
    			break;

    		case 'banner':
    			$path 	= Config('upload.banner');
    			break;

    		case 'news':
    			$path 	= Config('upload.news');
    			break;

    		case 'menu':
    			$path 	= Config('upload.menu');
    			break;

    		case 'configuration':
    			$path 	= Config('upload.configuration');
    			break;
    	}

    	return $path;
    }

    public static function generate_name($file_extension, $prefix = ""){
		$name = $prefix;
		for($i = 0; $i < 5; $i++){
			$name .= chr(rand(97, 122));
		}
		$name 	.= time();

		return $name . "." . $file_extension;
	}

	public static function getAllPicture($type, $dataPicture = '', $getFull = 1){
    	$arrReturn 	= [];

    	if($dataPicture){

    		$dataPicture 	= json_encode([['filename' => $dataPicture, 'is_main' => 1]]);
    		$arrReturn 		= self::getAllPictures($type, $dataPicture, $getFull);
    	}

    	return $arrReturn;
   	}

	public static function getAllPictures($type, $dataPicture = '', $getFull = 1){
    	$arrReturn 	= [];

    	if($dataPicture){
    		$temp 	= json_decode($dataPicture, true);

    		if($temp){
    			foreach ($temp as $value) {
					$filename	= isset($value['filename']) ? trim($value['filename']) : '';
					$is_main	= isset($value['is_main']) ? intval($value['is_main']) : 0;
					$url		= self::getUrlImage($type, $filename);
    				if(!empty($filename) && !empty($url)){
    					if($getFull == 1){
    						$arrReturn[] 	= ['filename' => $filename, 'url' => $url, 'is_main' => $is_main];
    					}else{
    						$arrReturn[] 	= $filename;
    					}
    				}
    			}
    		}
    	}

    	return $arrReturn;
   	}

   	public static function generatePictureJson($type, $dataPicture = [], $pictureMain = ''){
    	$arrReturn 	= [];
    	if($dataPicture){
			foreach ($dataPicture as $value) {
				$filename	= trim($value);
				$url			= self::getUrlImage($type, $filename);
				$is_main		= 0;
				if(!empty($filename) && !empty($url)){
					if($pictureMain != "" && $pictureMain == $filename) $is_main = 1;
					$arrReturn[] 	= ['filename' => $filename, 'url' => $url, 'is_main' => $is_main];
				}
			}
    	}

    	if($arrReturn) return json_encode($arrReturn);

    	return '';
   	}

	public static function resize_image($path, $filename, $maxwidth, $maxheight, $type = "small_", $new_path = "", $quality, $aspect_ratio = false){
		$sExtension = substr($filename, (strrpos($filename, ".") + 1));
		$sExtension = strtolower($sExtension);

		// Get new dimensions
		list($width, $height) = getimagesize($path . $filename);

		$new_width	= $maxwidth;
		$new_height	= $maxheight;
		if($aspect_ratio && $width != 0 && $height !=0){
			if($maxwidth / $width > $maxheight / $height) $percent = $maxheight / $height;
			else $percent = $maxwidth / $width;

			$new_width	= $width * $percent;
			$new_height	= $height * $percent;
		}


		// Resample
		$image_p = imagecreatetruecolor($new_width, $new_height);
		//check extension file for create
		switch($sExtension){
			case "gif":
				$image = imagecreatefromgif($path . $filename);
				break;
			case $sExtension == "jpg" || $sExtension == "jpe" || $sExtension == "jpeg":
				$image = imagecreatefromjpeg($path . $filename);
				break;
			case "png":
				$image = imagecreatefrompng($path . $filename);
				break;
		}
		//Copy and resize part of an image with resampling
		imagecopyresampled($image_p, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
		// Output

		// check new_path, nếu new_path tồn tại sẽ save ra đó, thay path = new_path
		if($new_path != "") $path = $new_path;

		switch($sExtension){
		case "gif":
			imagegif($image_p, $path . $type . $filename);
			break;
		case $sExtension == "jpg" || $sExtension == "jpe" || $sExtension == "jpeg":
			imagejpeg($image_p, $path . $type . $filename, $quality);
			break;
		case "png":
			imagepng($image_p, $path . $type . $filename);
			break;
		}
		imagedestroy($image_p);
	}
}
