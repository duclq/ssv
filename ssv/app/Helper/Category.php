<?php
namespace App\Helper;

use App\Repositories\CategoryRepository as Model;
use App\Transformers\CategoryTransformer;
use Request;
class Category
{
	public static $menu		= [];
	public static $arrType  = [];
    public function __construct(){

    }

    public static function getAllCategory($where = [], $group = true){

		$category 		= new Model;
		$data 			= $category->getAll($where);

 		$arrayCategory 	= [];
		foreach ($data as $key => $value) {
			$arrayCategory[$value['cat_parent_id']][$value['cat_id']]			=  $value;
			$arrayCategory[$value['cat_parent_id']][$value['cat_id']]["count"]	=  '';
		}

		self::sortLevel($arrayCategory, 0);

		$dataReturn	= [];
		if($group == true){
			$arrType 	= $category->getTypeLabels();
			foreach (self::$menu as $key => $value) {
				if(isset($arrType[$value['cat_type']])){
					if(!isset($dataReturn[$value['cat_type']])) $dataReturn[$value['cat_type']] = [];
					$dataReturn[$value['cat_type']][$key] 	= $value;
				}
			}
		}else{
			$dataReturn 	= self::$menu;
		}

		return $dataReturn;
    }

	public static function sortLevel($arrayCategory,$keystart=0,$level=-1){

		//kiểm tra xem tồn tại record không
		if(array_key_exists($keystart, $arrayCategory)){
			$level++;
			foreach($arrayCategory[$keystart] as $key=>$value){

				//gán các phần tử cho array menu sắp xếp theo đúng vị trí
				self::$menu[$value['cat_id']] 			= $value;

				//gan level cho menu
				self::$menu[$value['cat_id']]['level'] 	= $level;
				//thiet lap de biet day la` 1 nut cha
				if(array_key_exists($key, $arrayCategory)){
					self::$menu[$value['cat_id']]["cat_parent"] = 1;
				}else{
					self::$menu[$value['cat_id']]["cat_parent"] = 0;
				}

				//de quy de lap lai, neu menu_id man trong array cac menu cha
				self::sortLevel($arrayCategory,$key,$level);
			}
		}

	}
}
