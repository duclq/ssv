@extends('layout')
@push('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset(mix('css/pc/rewrite.css'), false) }}">
@endpush
@section('content')
    <div class="breadcrumb__list wrapper">
        <ul itemscope="" itemtype="http://schema.org/BreadcrumbList">
            <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                <a href="/việc-làm-tại-Bình-Định" itemprop="item"> <span itemprop="name"> Bình Định</span> <meta itemprop="position" content="2" /> </a>
            </li>
            <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                <a href="/việc-làm-tại-Huyện-Phù-Cát,Bình-Định" itemprop="item"> <span itemprop="name"> Huyện Phù Cát, Bình Định</span> <meta itemprop="position" content="3" /> </a>
            </li>
            <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                <a href="/nganh-nghe/việc-làm-kiến-trúc-thiết-kế-nội-thất-tại-Huyện-Phù-Cát,Bình-Định" itemprop="item">
                    <span itemprop="name">Kiến trúc - Thiết kế nội thất Huyện Phù Cát, Bình Định</span> <meta itemprop="position" content="4" />
                </a>
            </li>
            <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                <a href="https://123job.vn/viec-lam/nhan-vien-thiet-ke-ky-thuat-Adr8wd4A94" itemprop="item"> <span itemprop="name">NHÂN VIÊN THIẾT KẾ KỸ THUẬT</span> </a> <meta itemprop="position" content="5" />
            </li>
        </ul>
    </div>
    <section class="page page-rewrite">
        <section class="wrapper page-container">
            <section class="page-left">
                <h1>Tìm Việc Làm Nhân Viên Kinh Doanh Mới Nhất</h1>
                <div>Hiện có 24271 việc đang tuyển dụng</div>
                <div class="filter-selected">
                    <div>
                        <span>Mới đăng</span>
                        <a href="javacript:void(0)">
                            <i></i>
                        </a>
                    </div>
                </div>

                <div class="job-listing">
                    @for($i = 0; $i <= 15; $i++)
                    <div class="item">
                        <div class="logo">
                            <a href="" title="">
                                <img src="">
                            </a>
                        </div>
                        <div class="teaser">
                            <h2><a href="#" title="">Nhân Viên Kinh Doanh</a></h2>
                            <div class="company">CÔNG TY VINALINKS GROUP</div>
                            <div class="benfit grid-4">
                                <div class="location">Hồ Chí Minh</div>
                                <div class="salary">8 Tr - 12 Tr Vnd</div>
                            </div>
                            <p class="description">Số lượng: 03 ngườiGiới thiệu, tư vấn và thuyết phục các khách hàng sử dụng sản phẩm/dịch vụ của doanh nghiệpTìm kiếm, duy trì và phát triển mạng lưới khách hàng và đối ...</p>
                            <div class="more">
                                <div class="datetime">

                                </div>
                                <div class="job-save">
                                    <a href="#">
                                        <i></i>
                                        <span>Lưu việc</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endfor
                </div>

                <div class="pagenation">
                    <ul>
                        <li class="prev active"><a href="#" title="Trang trước">Trước</a></li>
                        @for ($i = 1; $i <= 6; $i++)
                            <li><a href="#" title="Trang {{ $i }}">{{ $i }}</a></li>
                        @endfor
                        <li class="next"><a href="#" title="Trang sau">Sau</a></li>
                    </ul>
                </div>
            </section>
            <section class="page-right">
                <div class="card">
                    <div class="card-heading">Mức lương</div>
                    <div class="card-content">
                        <ul class="locations">
                            <li><a href="#">Hà Nội</a></li>
                        </ul>
                        <a href="#" title="">Xem thêm</a>
                    </div>
                </div>

                <div class="card">
                    <div class="card-heading">Kinh nghiệm</div>
                    <div class="card-content">
                        <ul class="locations">
                            <li><a href="#">Hà Nội</a></li>
                        </ul>
                        <a href="#" title="">Xem thêm</a>
                    </div>
                </div>

                <div class="card">
                    <div class="card-heading">Cấp bậc</div>
                    <div class="card-content">
                        <ul class="locations">
                            <li><a href="#">Hà Nội</a></li>
                        </ul>
                        <a href="#" title="">Xem thêm</a>
                    </div>
                </div>

                <div class="card">
                    <div class="card-heading">Hình thức làm việc</div>
                    <div class="card-content">
                        <ul class="locations">
                            <li><a href="#">Hà Nội</a></li>
                        </ul>
                        <a href="#" title="">Xem thêm</a>
                    </div>
                </div>

                <div class="card">
                    <div class="card-heading">Ngành nghề</div>
                    <div class="card-content">
                        <ul class="locations">
                            <li><a href="#">Hà Nội</a></li>
                        </ul>
                        <a href="#" title="">Xem thêm</a>
                    </div>
                </div>
            </section>
        </section>
    </section>
@endsection
