@extends('layout')
@push('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset(mix('css/pc/detail.css'), false) }}">
@endpush
@section('content')
    <div class="breadcrumb__list wrapper">
        <ul itemscope="" itemtype="http://schema.org/BreadcrumbList">
            <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                <a href="/việc-làm-tại-Bình-Định" itemprop="item"> <span itemprop="name"> Bình Định</span> <meta itemprop="position" content="2" /> </a>
            </li>
            <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                <a href="/việc-làm-tại-Huyện-Phù-Cát,Bình-Định" itemprop="item"> <span itemprop="name"> Huyện Phù Cát, Bình Định</span> <meta itemprop="position" content="3" /> </a>
            </li>
            <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                <a href="/nganh-nghe/việc-làm-kiến-trúc-thiết-kế-nội-thất-tại-Huyện-Phù-Cát,Bình-Định" itemprop="item">
                    <span itemprop="name">Kiến trúc - Thiết kế nội thất Huyện Phù Cát, Bình Định</span> <meta itemprop="position" content="4" />
                </a>
            </li>
            <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                <a href="https://123job.vn/viec-lam/nhan-vien-thiet-ke-ky-thuat-Adr8wd4A94" itemprop="item"> <span itemprop="name">NHÂN VIÊN THIẾT KẾ KỸ THUẬT</span> </a> <meta itemprop="position" content="5" />
            </li>
        </ul>
    </div>

    <div class="detail">
        <div class="wrapper">
            <div class="content">
                <div class="job-info">
                    <p class="job-company">CÔNG TY CỔ PHẦN CÚC PHƯƠNG</p>
                    <h1 class="job-title">Công nhân sản xuất</h1>

                    <div class="job-apply-top">
                        <a href="#" class="btn-apply__now">Ứng tuyển ngay</a>
                        <a href="#">Lưu</a>
                        <a href="#">Chia sẻ</a>
                    </div>
                </div>

                <div class="job-property">
                    <div class="item job-place-work">
                        <i class="flaticon-location-pin"></i>
                        <span>Làm việc tại:</span>
                        <b>Hà Nội - Ba Đình</b>
                    </div>
                    <div class="item job-salary">
                        <i class="flaticon-money"></i>
                        <span>Mức lương:</span>
                        <b>6 - 8 triệu</b>
                    </div>
                    <div class="item">
                        <i class=""></i>
                        <span>Kinh nghiệm yêu cầu:</span>
                        <b>Không yêu cầu</b>
                    </div>
                    <div class="item">
                        <i class=""></i>
                        <span>Hình thức làm việc:</span>
                        <b>Không yêu cầu</b>
                    </div>
                    <div class="item">
                        <i class="flaticon-clock"></i>
                        <span>Hạn hết nhận hồ sơ:</span>
                        <b>19-02-2021</b>
                    </div>
                </div>
                <div class="content-group job-description">
                    <h2 class="content-group__title">Mô tả công việc</h2>
                    <div class="content-group__content">
                        <ul><li>Tham gia trực tiếp vào quá trình sản xuất sản phẩm </li><li>Thực hiện các công đoạn sản xuất theo quy trình sản xuất </li><li>Kiểm tra và đảm bảo chất lượng sản phẩm theo yêu cầu của quy trình, quy định công ty</li><li>Sắp xếp và vệ sinh đảm bảo khu vực sản xuất</li><li>Thực hiện các công việc khác theo yêu cầu của Quản lý trực tiếp.</li></ul>
                    </div>
                </div>
                <div class="content-group job-requirement">
                    <h2 class="content-group__title">YÊU CẦU CÔNG VIỆC</h2>
                    <div class="content-group__content">
                        <ul><li>Tham gia trực tiếp vào quá trình sản xuất sản phẩm </li><li>Thực hiện các công đoạn sản xuất theo quy trình sản xuất </li><li>Kiểm tra và đảm bảo chất lượng sản phẩm theo yêu cầu của quy trình, quy định công ty</li><li>Sắp xếp và vệ sinh đảm bảo khu vực sản xuất</li><li>Thực hiện các công việc khác theo yêu cầu của Quản lý trực tiếp.</li></ul>
                    </div>
                </div>
                <div class="content-group job-benfit">
                    <h2 class="content-group__title">QUYỀN LỢI CHI TIẾT</h2>
                    <div class="content-group__content">
                        <p>- Đóng BHXH, BHYT theo bộ luật lao động. Chế độ nghỉ lễ Tết, đi du lịch hằng năm.</p><p>- Được làm việc trong môi trường năng động, trẻ trung, đầy nhiệt huyết.</p><p>- Được tham gia các hoạt động team building và sự kiện lớn trong năm độc đáo, hấp dẫn.</p><p>- Cơ hội thăng tiến, phát triển cao.</p>
                    </div>
                </div>
                <div class="content-group job-summary">
                    #<b>Tóm tắt:</b> Bạn đang xem tin <b> Việc làm Công nhân sản xuất</b> trong ngành Lao động phổ thông, Cơ khí - Chế tạo, Kỹ thuật được tuyển dụng tại <b>Hưng Yên: Văn Giang </b>bởi <b>Công ty cổ phần cúc phương</b>. <b>Công ty cổ phần cúc phương</b> đang cần tuyển trên 20 người nhân sự với hình thức làm việc: Toàn thời gian cố định. yêu cầu kinh nghiệm Không yêu cầu. Website tìm việc làm 123job.vn cập nhật tin <b>Công nhân sản xuất</b> cách đây lúc 2021-02-19 19:52:28. <b>Người tìm việc lưu ý không nên đặt tiền cọc khi xin việc</b>
                </div>
                <div class="job-apply-now">
                    <div class="job-save">

                    </div>
                    <div class="job-btn-apply-now">

                    </div>
                </div>
                <div class="job-recommend">

                </div>
                <div class="content-group job-keyword-refer">
                    <h2 class="content-group__title">Tìm kiếm theo từ khóa</h2>
                    <div class="content-group__content">
                        <h3 class="refer-item">
                            <a href="" title="Việc làm Công ty cổ phần cúc phương">Việc làm Công ty cổ phần cúc phương</a>
                        </h3>
                        <h3 class="refer-item">
                            <a href="" title="Việc làm Công ty cổ phần cúc phương">Việc làm Công ty cổ phần cúc phương</a>
                        </h3>
                    </div>
                </div>
                <div class="job-note">
                    <div><b>Lưu ý:</b></div>
                    <p>Người tìm việc đang xem tin tuyển dụng <b>Công nhân sản xuất</b> tại <b>Hưng Yên: Văn Giang</b>.
                        Mọi thông tin liên quan tới tin tuyển dụng này là do người đăng tin đăng tải và chịu trách nhiệm. Chúng tôi luôn
                        cố gắng để có chất lượng thông tin tốt nhất, nhưng chúng tôi không đảm bảo và không chịu trách nhiệm về bất kỳ
                        nội dung nào liên quan tới tin việc làm này. Nếu người tìm việc phát hiện có sai sót hay vấn đề gì xin hãy <a href="javascript:void(0)" class="js-show-report text-pink">thông báo cho chúng tôi</a> </p>
                </div>
            </div>
            <aside class="sidebar">
                <div class="card card-company">
                    <div class="job-company-info">
                        <div class="company-info__avatar">
                            <img src="" alt="">
                        </div>
                        <h3 class="company-info__name">
                            Công ty A
                        </h3>
                        <div class="company-info__review">
                            <div class="created">(0 Đánh giá công ty )</div>
                            <div class="star-review"> <div class="star-icon">
                                    <span class="star-rate" style="width: 0%;">
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="company-info__description">
                            <p> Công ty Cổ Phần Cúc Phương hoạt động trong lĩnh vực sản xuất, và cung cấp giải pháp toàn diện cho cá... </p>
                        </div>
                        <div class="company-info__property">
                            <ul>
                                <li>
                                    <i class="flaticon-location-pin"></i>
                                    <span>54A Nguyễn Chí Thanh, quận Đống Đa, Hà Nội</span>
                                </li>
                                <li>
                                    <i class="flaticon-user"></i>
                                    <span>100 - 200 nhân sự</span>
                                </li>
                            </ul>
                        </div>
                        <div class="company-info__jobs">
                            <div class="jobs-box__small">
                                <div class="jobs-box__title">Đang tuyển</div>
                                <div class="jobs-box__container">
                                    <div class="jobs-box__item">
                                        <h3><a href="#">Tuyển nhân sự a</a></h3>
                                        <div class="jobs-box__item__property">
                                            <ul class="grid-3">
                                                <li>
                                                    <i></i>
                                                    <span>35tr</span>
                                                </li>
                                                <li>
                                                    <i></i>
                                                    <span>Hà Nội</span>
                                                </li>
                                                <li>
                                                    <i></i>
                                                    <span>30-12-2021</span>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="jobs-box__item">
                                        <h3><a href="#">Tuyển nhân sự a</a></h3>
                                        <div class="jobs-box__item__property">
                                            <ul class="grid-3">
                                                <li>
                                                    <i></i>
                                                    <span>35tr</span>
                                                </li>
                                                <li>
                                                    <i></i>
                                                    <span>Hà Nội</span>
                                                </li>
                                                <li>
                                                    <i></i>
                                                    <span>30-12-2021</span>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-heading">Việc làm theo địa chỉ</div>
                    <div class="card-content">
                        <ul class="locations">
                            <li><a href="#">Hà Nội</a></li>
                        </ul>
                    </div>
                </div>

                <div class="card">
                    <div class="card-heading">Việc làm tương tự</div>
                    <div class="card-content">
                        <div class="jobs-box__small">
                            <div class="jobs-box__container">
                                <div class="jobs-box__item">
                                    <h3><a href="#">Tuyển nhân sự a</a></h3>
                                    <div class="jobs-box__item__property">
                                        <ul class="grid-3">
                                            <li>
                                                <i></i>
                                                <span>35tr</span>
                                            </li>
                                            <li>
                                                <i></i>
                                                <span>Hà Nội</span>
                                            </li>
                                            <li>
                                                <i></i>
                                                <span>30-12-2021</span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="jobs-box__item">
                                    <h3><a href="#">Tuyển nhân sự a</a></h3>
                                    <div class="jobs-box__item__property">
                                        <ul class="grid-3">
                                            <li>
                                                <i></i>
                                                <span>35tr</span>
                                            </li>
                                            <li>
                                                <i></i>
                                                <span>Hà Nội</span>
                                            </li>
                                            <li>
                                                <i></i>
                                                <span>30-12-2021</span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </aside>
        </div>
    </div>
@endsection
