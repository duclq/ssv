@extends('layout')
@push('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset(mix('css/pc/style.css'), false) }}">
@endpush
@section('content')
    @include('layout.banner')

    <section class="job-section job-trend mg-tb-20 wrapper">
        <section class="job-section-head">
            <h2>Xu hướng nghề nghiệp</h2>
            <p>Xu hướng nghề nghiệp tìm kiếm phổ biến</p>
        </section>

        <section class="job-section-container">
            <section class="job-trend-list">
                @for ($i = 0; $i <= 8; $i++)
                    <section class="job-trend-item">
                        <i class="flaticon-antenna"></i>
                        <section class="job-trend-item-teaser">
                            <h3><a href="#" title="Công nghệ thông tin">Công nghệ thông tin</a></h3>
                            <p><b>2</b> vị trí</p>
                        </section>
                    </section>
                @endfor
            </section>
        </section>
    </section>

    <section class="job-section job-new mg-tb-20 wrapper">
        <section class="job-section-head">
            <h2>Tin tuyển dụng, việc làm mới nhất</h2>
            <p>Tuyển dụng, việc làm 24h từ công ty uy tín hàng đầu Việt Nam</p>
        </section>

        <div class="job-section-container">
            <div class="job-list grid-2">
                @for ($i = 0; $i <= 8; $i++)
                <div class="job-list-item">
                    <article class="job-card">
                        <div class="flex-middle">
                            <div class="employer-logo">
                                <a href="https://apusthemes.com/wp-demo/careerup/job/junior-digital-graphic-designer/">
                                    <img src="https://apusthemes.com/wp-demo/careerup/wp-content/uploads/2019/05/brand7-180x75.jpg" class="attachment-thumbnail size-thumbnail wp-post-image" alt="">                            </a>
                            </div>

                            <div class="job-information">
                                <div class="job-title-wrapper">
                                    <h2 class="job-title"><a href="#" rel="bookmark" title="">Junior Digital Graphic Designer</a></h2>
                                    <h3 class="job-company"><a href="#" rel="bookmark" title="">Công ty FPT</a></h3>
                                </div>
                                <div class="job-metas grid-3">
                                    <div class="job-location"><i class="flaticon-location-pin"></i>Brooklyn, NY USA</div>
                                    <div class="job-salary"><i class="flaticon-price"></i>$<span class="price-text">12</span> - $<span class="price-text">18</span> per hour</div>
                                </div>

                            </div>
                        </div>
                    </article>
                </div>
                @endfor
            </div>
        </div>
    </section>

    <section class="job-section job-cate mg-tb-20 wrapper">
        <section class="job-section-head">
            <h2>Việc làm theo ngành nghề</h2>
            <p>Tuyển dụng, việc làm 24h từ công ty uy tín hàng đầu Việt Nam</p>
        </section>

        <section class="job-section-container">
            <section class="job-carrier">
                <ul class="grid-4">
                    <li>
                        <a href="#">Kinh doanh</a>
                    </li>
                    <li>
                        <a href="#">Kinh doanh</a>
                    </li>
                    <li>
                        <a href="#">Kinh doanh</a>
                    </li>
                    <li>
                        <a href="#">Kinh doanh</a>
                    </li>
                    <li>
                        <a href="#">Kinh doanh</a>
                    </li>
                    <li>
                        <a href="#">Kinh doanh</a>
                    </li><li>
                        <a href="#">Kinh doanh</a>
                    </li>
                    <li>
                        <a href="#">Kinh doanh</a>
                    </li>
                    <li>
                        <a href="#">Kinh doanh</a>
                    </li>
                </ul>
            </section>
        </section>
    </section>

    <section class="job-section job-cate mg-tb-20 wrapper">
        <section class="job-section-head">
        <h2>Cẩm nang kiếm việc làm 24h</h2>
        <p>---</p>
        </section>
    </section>

@endsection
