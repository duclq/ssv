<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>So Sánh Việc</title>

    @stack('styles')

</head>
<body>

@include('layout.header')

@yield('content')

@include('layout.news_letter')

@include('layout.footer')

<script src="{{ asset('js/jquery-3.5.1.min.js') }}"></script>
<script src="{{ asset('js/customs.js') }}"></script>
</body>
</html>
