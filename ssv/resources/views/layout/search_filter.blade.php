<section class="search-fillter">
    <form action="" method="">
        <section class="field-search type-text mg-r-20">
            <i class="flaticon-search"></i>
            <input autocomplete="off" placeholder="IT, PHP, Ké toán ..." class="none" type="search" name="s">
        </section>

        <section class="job-select-regions mg-r-20">
            <input type="hidden" >
            <label>
                <i class="flaticon-location-pin"></i>
                <span>Toàn quốc</span>
            </label>
            <section class="regions-list-search">
                <input type="search" onkeyup="myFunction(this)" placeholder="Tìm kiếm ...">
                <section class="regions-list-link">
                    <ul>
                        <li><a href="javascript:void(0)" data-id="0" rel="nofollow">Miền Bắc</a></li>
                        <li><a href="javascript:void(0)" data-id="1" rel="nofollow">TP.Hà Nội</a></li>
                        <li><a href="javascript:void(0)" data-id="2" rel="nofollow">TP.Hồ Chí Minh</a></li>
                    </ul>
                    <ul>
                        <li><a href="javascript:void(0)" data-id="0" rel="nofollow">Miền Trung</a></li>
                        <li><a href="javascript:void(0)" data-id="1" rel="nofollow">TP.Hà Nội</a></li>
                        <li><a href="javascript:void(0)" data-id="2" rel="nofollow">TP.Hồ Chí Minh</a></li>
                    </ul>
                    <ul>
                        <li><a href="javascript:void(0)" data-id="0" rel="nofollow">Miền Nam</a></li>
                        <li><a href="javascript:void(0)" data-id="1" rel="nofollow">TP.Hà Nội</a></li>
                        <li><a href="javascript:void(0)" data-id="2" rel="nofollow">TP.Hồ Chí Minh</a></li>
                    </ul>
                </section>
            </section>
        </section>

        <button type="submit" class="btn-submit-search">
            <i class="ic-search"></i>
            <span>Tìm kiếm</span>
        </button>
    </form>

    <section class="job-trending-keyword mg-t-20">
        <span>Tìm kiếm nhiều:</span>
        <section class="job-trending-keyword-list">
            <a href="#" title="">PHP</a>
            <a href="#" title="">JS</a>
        </section>
    </section>
</section>
