<section class="banner">
    <section class="wrapper">
        <section class="banner-slogan">
            <h1>Tìm việc làm tốt nhất</h1>
            <p>Hàng ngàn cơ hội việc làm đang chờ bạn, nhà tuyển dụng luôn sãn sàng đón nhận cv từ các ứng viên</p>
        </section>
        @include('layout.search_filter')
    </section>
</section>
