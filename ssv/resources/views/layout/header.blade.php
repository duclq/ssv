<header>
    <section class="wrapper header">
        <div class="logo-top">
            <a href="/" title=""><img src="{{ asset('images/logo.png') }}"></a>
        </div>

        <div class="header-right">
            <nav class="nav nav-top mg-r-20">
                <ul role="menu">
                    <li class="mg-r-20">
                        <a href="#" title="">Việc làm</a>
                        <ul>
                            <li><a href="" title="">A</a></li>
                            <li><a href="" title="">A</a></li>
                            <li><a href="" title="">A</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#" title="">Tư vấn</a>
                    </li>
                </ul>
            </nav>

            <a href="" rel="nofollow" class="mg-r-20">
                <i></i>
                <span>Đăng tin</span>
            </a>

            <a href="" rel="nofollow" class="mg-r-20">Đăng nhập</a>
            <a href="" rel="nofollow">Đăng ký</a>

        </div>
    </section>
</header>
