<footer class="footer">
    <section class="footer-container">
        <section class="wrapper footer-list-rewrite">
            <nav class="job-list-link">
                <ul>
                    <li><a href="#" title="Lập trình viên NodeJS">Lập trình viên NodeJS</a></li>
                    <li><a href="#" title="Lập trình viên NodeJS">Lập trình viên NodeJS</a></li>
                </ul>
            </nav>

            <nav>
                <h4>Việc làm tại Hồ Chí Minh</h4>
                <ul>
                    <li></li>
                </ul>
            </nav>

            <nav>
                <h4>Tuyển dụng toàn quốc</h4>
                <ul>
                    <li></li>
                </ul>
            </nav>

            <nav>
                <h4>Công ty tuyển dụng</h4>
                <ul>
                    <li></li>
                </ul>
            </nav>
        </section>
    </section>

    <div class="wrapper coppy-right">
        &reg; 2021
    </div>
</footer>
