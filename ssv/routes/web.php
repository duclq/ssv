<?php

use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('pages.home');
})->name('home');

Route::get('/rewrite', function () {
    return view('pages.rewrite');
})->name('rewrite');

Route::get('/detail', function () {
    return view('pages.detail');
})->name('detail');

Route::get('/search', function () {
    return view('pages.search');
})->name('search');
