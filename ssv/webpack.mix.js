const mix = require('laravel-mix');
mix.browserSync('127.0.0.1:8080');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.sass('public/less/pc/_style.scss', 'public/css/pc/style.css');
mix.sass('public/less/pc/_detail.scss', 'public/css/pc/detail.css');
mix.sass('public/less/pc/_rewrite.scss', 'public/css/pc/rewrite.css');
